package com0oky.httpkit.test;

import org.apache.http.concurrent.FutureCallback;

/**
 * @author mdc
 * @date 2017年6月11日
 */
public class MyFutureCallback implements FutureCallback<String> {

	@Override
	public void completed(String result) {
		System.out.println("completed:" + result);
	}

	@Override
	public void failed(Exception ex) {
		System.out.println("failed:" + ex.getMessage());
		ex.printStackTrace();
	}

	@Override
	public void cancelled() {
		System.out.println("cancelled");
	}

}
