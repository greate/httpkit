/**
 * @author mdc
 * @date 2017年7月5日 下午1:12:55
 */
package com0oky.httpkit.test;

import java.io.File;

import org.junit.Ignore;
import org.junit.Test;

import com0oky.httpkit.http.HttpKit;
import com0oky.httpkit.http.request.FormPart;

/**
 * @author mdc
 * @date 2017年7月5日 下午1:12:55
 */
public class UploadFileTest {
	
	@Test
	//只是演示, 忽略此测试
	@Ignore
	public void uploadFileTest(){
		FormPart form = FormPart.create()
		// 添加额外参数
		.addParameter("age", 40)
		//添加文件
		.addParameter("fileName", new File("d:/test.txt"));
		
		String result = HttpKit.post("http://www.test.com").useForm(form).execute().getString();
		System.out.println(result);
	}
	
	//获取oschina 验证码
	@Test
	public void downloadImgTest(){
		HttpKit.get("https://www.oschina.net/action/user/captcha?t=0.1058898605559051")
		.setUserAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36")
		.addHeader("Referer", "https://www.oschina.net/home/reg?goto_page=https%3A%2F%2Fwww.oschina.net%2F")
		.execute().transferTo("d:/img.png");
	}
}
